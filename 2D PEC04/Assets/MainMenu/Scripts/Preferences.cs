using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Preferences : MonoBehaviour
{

    #region Singleton
    public static Preferences instance;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("GameManager is already instantiated.");
        }

        DontDestroyOnLoad(gameObject);

    }


    #endregion



    [SerializeField]
    float fontSize = 10;


}
