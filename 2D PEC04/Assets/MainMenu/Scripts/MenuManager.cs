using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Audio;

/*Script for the main menu scene
 * Controls many canvas layers
 * and the button functions
 */

public class MenuManager : MonoBehaviour
{
    [Header("Components")]
    [SerializeField]
    GameObject titleScreen;
    [SerializeField]
    TextMeshProUGUI startGameText;

    [SerializeField]
    GameObject menuHUD;

    [SerializeField]
    Image foreground;
    [SerializeField]
    GameObject settingsPanel;

    [Header("Audio Settings")]
    [SerializeField]
    public AudioMixer audioMixer;


    public int generalVolume = 10;
    public int backGroundVolume = 10;
    public int fxVolume = 10;



    bool interactible = false;

    float fadeDuration = 2;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(MenuBegins());
    }

    // Update is called once per frame
    void Update()
    {


        if (Input.anyKeyDown && interactible)
        {
            startGameText.gameObject.SetActive(false);
            StartCoroutine(TakeOffForeground());
        }


    }

    //Open menu
    IEnumerator MenuBegins()
    {
        yield return new WaitForSeconds(2);

        startGameText.gameObject.SetActive(true);
        interactible = true;
    }

    //Hide the men� layer in the title
    IEnumerator TakeOffForeground()
    {
        Color initialColor = foreground.color;
        Color targetColor = new Color(initialColor.r, initialColor.g, initialColor.b, 0);
        float elapsedTime = 0f;

        while(elapsedTime < fadeDuration)
        {
            elapsedTime += Time.deltaTime;
            foreground.color = Color.Lerp(initialColor, targetColor, elapsedTime / fadeDuration);
            yield return null;

        }

        foreground.gameObject.SetActive(false);

    }



    //Button controls
    public void OnStartGame()
    {

        SceneManager.LoadScene(1);
    }
    public void OnSettingButton()
    {
        settingsPanel.SetActive(!settingsPanel.activeSelf);
    }


    //Options about soun using audioMixer and sliders
    public void SetGeneralVolume(float volume)
    {
        audioMixer.SetFloat("MasterVolume", volume);
    }
    public void SetBackgroundVolume(float volume)
    {
        audioMixer.SetFloat("BackgroundVolume", volume);
    }

    public void SetFXVolume(float volume)
    {
        audioMixer.SetFloat("FXVolume", volume);
    }


}
