using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogue3 : MonoBehaviour
{
    DialogueSystem dialogue;

    [SerializeField]
    GameObject answersPanel;

    [SerializeField]
    GameObject questions2;

    [SerializeField]
    GameObject dialogueA;
    [SerializeField]
    GameObject dialogueB;




    // Start is called before the first frame update
    void Start()
    {
        dialogue = DialogueSystem.instance;
        addLine();
    }


    public string[] s = new string[]
    {

    };

    int index = 0;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0))
        {
            addLine();
        }
    }


    public void addLine()
    {
        if (!dialogue.isSpeaking || dialogue.isWaitingForUserInput)
        {
            if (index >= s.Length)
            {

                Debug.Log("se acab� el dialogo");
                answersPanel.SetActive(true);
                questions2.SetActive(true);

                return;
            }





            Say(s[index]);
            index++;
        }


    }


    void Say(string s)
    {
        string[] parts = s.Split(':');
        string speech = parts[0];
        string speaker = (parts.Length > 1) ? parts[1] : "";

        dialogue.Say(speech, speaker);
    }



    public void OnAnswerA()
    {

        dialogueA.SetActive(true);
        answersPanel.SetActive(false);
        questions2.SetActive(false);
        this.gameObject.SetActive(false);

    }

    public void OnAnswerB()
    {

        dialogueB.SetActive(true);
        answersPanel.SetActive(false);
        questions2.SetActive(false);
        this.gameObject.SetActive(false);

    }







}
