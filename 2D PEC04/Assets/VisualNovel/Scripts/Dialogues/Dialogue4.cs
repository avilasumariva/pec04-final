using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Dialogue4 : MonoBehaviour
{
    DialogueSystem dialogue;



    [SerializeField]
    Image background;

    [SerializeField]
    GameObject characters;

    // Start is called before the first frame update
    void Start()
    {
        dialogue = DialogueSystem.instance;

        background.sprite = null;
        background.color = Color.black;

        characters.SetActive(false);
        addLine();
    }


    public string[] s = new string[]
    {

    };

    int index = 0;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0))
        {
            addLine();
        }
    }


    public void addLine()
    {
        if (!dialogue.isSpeaking || dialogue.isWaitingForUserInput)
        {
            if (index >= s.Length)
            {
                Debug.Log("se acab� el dialogo");
                SceneManager.LoadScene(3);
                return;
            }
            Say(s[index]);
            index++;
        }


    }


    void Say(string s)
    {
        string[] parts = s.Split(':');
        string speech = parts[0];
        string speaker = (parts.Length > 1) ? parts[1] : "";

        dialogue.Say(speech, speaker);
    }










}
