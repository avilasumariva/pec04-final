using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogue1 : MonoBehaviour
{
    DialogueSystem dialogue;

    [SerializeField]
    GameObject answersPanel;

    [SerializeField]
    GameObject questions1;

    [SerializeField]
    GameObject dialogueA;
    [SerializeField]
    GameObject dialogueB;


    [SerializeField]
    GameObject character1;
    [SerializeField]
    GameObject character2;



    // Start is called before the first frame update
    void Start()
    {
        dialogue = DialogueSystem.instance;
    }


    public string[] s = new string[]
    {
   
    };

    int index = 0;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (!dialogue.isSpeaking || dialogue.isWaitingForUserInput)
            {
                if (index >= s.Length)
                {

                    Debug.Log("se acab� el dialogo");
                    answersPanel.SetActive(true);
                    questions1.SetActive(true);

                    return;
                }




                CheckCharacter();


                Say(s[index]);
                index++;
            }
        }
    }


    public void CheckCharacter() {

        switch (index)
        {
            case 2:
                character1.SetActive(true);
                break;



            case 4:
                character2.SetActive(true);
                break;
     
        }


        
    }


    void Say(string s)
    {
        string[] parts = s.Split(':');
        string speech = parts[0];
        string speaker = (parts.Length > 1) ? parts[1] : "";

        dialogue.Say(speech, speaker);
    }



    public void OnAnswerA()
    {

        dialogueA.SetActive(true);
        questions1.SetActive(false);
        answersPanel.SetActive(false);

        this.gameObject.SetActive(false);

    }

    public void OnAnswerB()
    {
        dialogueB.SetActive(true);
        questions1.SetActive(false);
        answersPanel.SetActive(false);

        this.gameObject.SetActive(false);

    }







}
