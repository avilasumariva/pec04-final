using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnersManager : MonoBehaviour
{


    [SerializeField]
    List<EnemySpawner> enemySpawners;
    // Start is called before the first frame update


    public void StartAttack()
    {
        foreach(EnemySpawner es in enemySpawners)
        {
            es.StartSpawn();
        }


    }

    public void StopAttack()
    {
        foreach (EnemySpawner es in enemySpawners)
        {
            es.StopSpawn();
        }
    }



}
