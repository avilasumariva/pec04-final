using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*Script from the plane of the player
 * has the parameters of the vehicle
 */
public class Skyship : MonoBehaviour
{
    [SerializeField]
    GameObject model;

    [SerializeField]
    int life = 5;

    [SerializeField]
    float xSpeed = 10;

    [SerializeField]
    float ySpeed = 10;

    Rigidbody2D rb;

    float rotate = 50.0f;
    [SerializeField]
    string weaknessTag;


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if(collision.tag == weaknessTag)
        {

            Hurt(1);
            Debug.Log("AY");
        }

    }


    void Hurt(float damage)
    {

        Debug.Log("Me dio");
        life -= 1;

        if(life < 0)
        {

            Die();

        }

        UIManager.instance.refreshLife(life);


    }

    void Die()
    {


        Debug.Log("dead");

        GameManager.instance.GameOver();

    }


}
