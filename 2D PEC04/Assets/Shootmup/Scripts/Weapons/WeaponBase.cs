using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBase : MonoBehaviour
{

    [SerializeField]
    GameObject projectile;

    [SerializeField]
    protected float speedCadence;

    [SerializeField]
    int ammo;

    [SerializeField]
    Transform firePoint;
    [SerializeField]
    float shootForce;

    [SerializeField]
    protected bool canShoot = true;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Fire()
    {
        GameObject shotProjectile = Instantiate(projectile, firePoint.position, firePoint.rotation);
        //shotProjectile.GetComponent<Rigidbody2D>().AddForce(firePoint.up * shootForce, ForceMode2D.Impulse);


    }

    public virtual void WeaponTrigger()
    {
        Debug.Log("Implementar el control del disparo");
    }

}
