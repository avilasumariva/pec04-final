using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicGun : WeaponBase
{
    public override void WeaponTrigger()
    {

        if (Input.GetKey(KeyCode.Space) && canShoot)
        {
            canShoot = false;
            StartCoroutine(shootRate());
            this.Fire();


        }


    }

    IEnumerator shootRate()
    {
        yield return new WaitForSeconds(speedCadence);
        canShoot = true;
    }
}
