using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    [SerializeField]
    float damage;

    Rigidbody2D rb;
    
    [SerializeField]
    float speed = 20;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        rb.velocity = Vector2.up * speed;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "BulletLimit")
        {

            Destroy(gameObject);

        }
    }


    public float getDamage()
    {
        return damage;
    }


}
