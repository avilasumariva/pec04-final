using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField]
    GameObject model;

    [SerializeField]
    float life = 5;

    Rigidbody2D rb;

    [SerializeField]
    float moveSpeed = 3;
    [SerializeField]
    int punctuation = 100;

    [SerializeField]
    ParticleSystem hitParticles;

    [SerializeField]
    ParticleSystem dieParticles;

    [SerializeField]
    float limits = 11;



    // Update is called once per frame
    void Update()
    {

        transform.Translate(0, moveSpeed * Time.deltaTime, 0);


        if(Mathf.Abs(transform.position.x) > limits || Mathf.Abs(transform.position.y) > limits)
        {
            Destroy(gameObject);
        }


    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.tag != null || collision.tag != ""))
        {

            if (collision.tag == "bullet")
            {
                Hurt(collision.GetComponent<Bullet>().getDamage());

                Destroy(collision.gameObject);

            }
        }
    }


    void Hurt(float damage)
    {
        life -= damage;


        if (life < 0)
        {

            Die();

        }

        Instantiate(hitParticles, transform.position, Quaternion.identity);


    }

    void Die()
    {
        Instantiate(dieParticles, transform.position, Quaternion.identity);

        GameManager.instance.AddPunctuation(punctuation);
        Debug.Log("dead");
        Destroy(gameObject);

    }


}
