using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileControl : MonoBehaviour
{

    float speed = 3;

    bool isMoving=true;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (isMoving)
        {
            transform.Translate(0, -speed * Time.deltaTime, 0);


        }

        if (transform.position.y <= -23)
        {
            transform.position = new Vector2(0, 17);
        }


    }
}
