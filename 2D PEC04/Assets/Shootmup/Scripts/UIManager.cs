using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public class UIManager : MonoBehaviour
{
    [SerializeField]
    List<Image> lifes;


    [SerializeField]
    TextMeshProUGUI punctuationText;


    [SerializeField]
    GameObject pausePanel;

    [SerializeField]
    GameObject gameOverPanel;


    #region Singleton
    public static UIManager instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("GameManager is already instantiated.");
        }
    }
    #endregion


    public void refreshLife(int life)
    {
        lifes[life].color = Color.red;
        
    }

    public void refreshPunctuation(int punct)
    {
        punctuationText.text = punct.ToString();

    }


    public void OnPause()
    {
        pausePanel.SetActive(!pausePanel.activeSelf);
    }


    public void OnGameOver()
    {
        gameOverPanel.SetActive(true);
    }


}
