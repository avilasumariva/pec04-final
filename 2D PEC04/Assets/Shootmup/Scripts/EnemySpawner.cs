using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    [SerializeField]
    GameObject prefab;

    [SerializeField]
    int number = 3;

    [SerializeField]
    float waitTime = 3;

    [SerializeField]
    float frequency;
    // Start is called before the first frame update
    [SerializeField]
    bool spawning = true;

    public void StartSpawn()
    {
        spawning = true;
        StartCoroutine(Spawn());
    }

    public void StopSpawn()
    {
        spawning = false;
        StopAllCoroutines();
    }

    IEnumerator Spawn()
    {
        if (spawning)
        {
            for (int i = 0; i < number; i++)
            {
                Instantiate(prefab, transform.position, transform.rotation);
                yield return new WaitForSeconds(frequency);

            }
            StartCoroutine(WaitRoutine());
        }
    }

    IEnumerator WaitRoutine()
    {

        yield return new WaitForSeconds(frequency);
        StartCoroutine(Spawn());
    }
}
