using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    Rigidbody2D rb;
    [Header("Model")]
    [SerializeField]
    Skyship plane;
    [SerializeField]
    GameObject model;

    [Header("Variables")]

    [SerializeField]
    float moveSpeed = 10.0f;

    [SerializeField]
    float rotationSpeed;

    float horizontalMovement;
    float verticalMovement;
    [SerializeField]
    WeaponBase weapon;


    [SerializeField]
    float xLimits;

    [SerializeField]
    float yLimits;
    
    float rotate = 50.0f;

    [SerializeField]
    bool canBeHurt = true;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        getWeapon();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();

        weapon.WeaponTrigger();  

    }


    void Movement()
    {

        horizontalMovement = Input.GetAxisRaw("Horizontal") * moveSpeed * Time.deltaTime;
        verticalMovement = Input.GetAxisRaw("Vertical") * moveSpeed * Time.deltaTime;

 

        transform.Translate(horizontalMovement, verticalMovement, 0);
        model.transform.eulerAngles = Vector2.up * rotate * Input.GetAxisRaw("Horizontal");
        //transform.position = new Vector2(transform.position.x + horizontalMovement, transform.position.y);

    }


    public void getWeapon()
    {
        weapon = GetComponentInChildren<WeaponBase>();
    }

}
