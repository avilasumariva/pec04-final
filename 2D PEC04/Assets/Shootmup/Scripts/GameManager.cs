using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*Main manager from shooter scene
 * Controls the game state. Punctuation, enemy spawner, and communicates with the IU through IUManager
 */
public class GameManager : MonoBehaviour
{

    //Stablish instance
    #region Singleton
    public static GameManager instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("GameManager is already instantiated.");
        }
    }
    #endregion

    [SerializeField]
    GameState gameState;

    [SerializeField]
    int punctuation = 0;

    [SerializeField]
    int goal = 1000;

    [SerializeField]
    SpawnersManager spawner;

    [Header("Dialogue")]
    [SerializeField]
    GameObject dialoguePanel;
    [SerializeField]
    GameObject dialogue1;
    [SerializeField]
    GameObject dialogue2;


    [Header("Audio resources")]
    [SerializeField]
    AudioSource audioSource;
    [SerializeField]
    AudioClip dialogueMusic;
    [SerializeField]
    AudioClip battleMusic;

    GameState resumeState;


    private void Start()
    {
        //prevents the restart function
        Time.timeScale = 1;
        audioSource.pitch = 1;
    }

    private void Update()
    {

        //active Pause state
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(gameState!=GameState.PAUSE)
                OnPauseButton();
        }
    }
    public void AddPunctuation(int punct)
    {
        //enemies communicate the points when they died
        punctuation += punct;
        UIManager.instance.refreshPunctuation(punctuation);
        
        CheckOver();
    }

    //compare the punctuation with the goal
    public void CheckOver()
    {
        if (punctuation >= goal)
        {
            FinishCombat();
            Debug.Log("FInito");
        }

    }

    //Desactivate dialogue mode
    public void CombatBegins()
    {
        audioSource.clip = battleMusic;
        audioSource.Play();

        dialogue1.SetActive(false);
        dialoguePanel.SetActive(false);
        spawner.StartAttack();
        gameState = GameState.COMBAT;

    }

    //Reactivate dialogue
    public void FinishCombat()
    {
        audioSource.clip = dialogueMusic;
        audioSource.Play();
        spawner.StopAttack();
        gameState = GameState.DIALOGUE;

        dialoguePanel.SetActive(true);
        dialogue2.SetActive(true);

    }
    //Button control functions
    public void OnPauseButton()
    {
            resumeState = gameState;
            gameState = GameState.PAUSE;

            Time.timeScale = 0;
            UIManager.instance.OnPause();
    }

    public void OnResumeButton()
    {
        gameState = resumeState;
        Time.timeScale = 1;
        UIManager.instance.OnPause();
    }

    public void OnRestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OnExitGame()
    {
        Application.Quit();
    }

    public void EndGame()
    {
        SceneManager.LoadScene(3);
    }

    public void GameOver()
    {
        UIManager.instance.OnGameOver();
        Time.timeScale = 0;
        gameState = GameState.GAME_OVER;
        audioSource.pitch = 0.5f;
    }


}

enum GameState
{
    DIALOGUE,
    COMBAT,
    PAUSE,
    GAME_OVER
}
