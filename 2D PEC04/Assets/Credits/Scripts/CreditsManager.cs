using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class CreditsManager : MonoBehaviour
{
    public static CreditsManager instance;

    public Canvas moveCanvas;
    public List<CreditsEnemy> listNames;

    public Canvas staticCanvas;
    public GameObject detailsPanel;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI detailText;

    public bool isMoving;
    public float moveSpeed = 3;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PanelMove();
    }


    public void PanelMove()
    {
        if (isMoving)
        {

            if(moveCanvas.transform.position.y <-20)
            {
                isMoving = false;
                EndGame();

            }
            moveCanvas.transform.Translate(0, -moveSpeed * Time.deltaTime, 0);
        }

    }


    public void fillDetailsPanel(string _name, string _details)
    {
        if (!detailsPanel.activeSelf)
        {
            detailsPanel.SetActive(true);
        }

        this.nameText.text = _name;
        detailText.text = _details;


    }



    public void EndGame()
    {
        StartCoroutine(GoToMenu());


    }


    IEnumerator GoToMenu()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(0);


    }

}
