using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

//Interactive canvas object
[System.Serializable]
public class CreditsEnemy : MonoBehaviour
{
    [SerializeField]
    float life = 3;
    TextMeshProUGUI tmp;

    [SerializeField]
    string name;
    [SerializeField]
    string details;

    [SerializeField]
    Color color;
    // Start is called before the first frame update
    void Start()
    {
        tmp = GetComponent<TextMeshProUGUI>();
        tmp.text = name;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("bullet"))
        {

            Destroy(collision.gameObject);
            Hit();


        }
    }


    public void Hit()
    {
        life -= 1;

        if (life <= 0)
        {
            tmp.color = color;
            GetComponent<BoxCollider2D>().enabled = false;

            CreditsManager.instance.fillDetailsPanel(name,details);

        }


    }


}
