# PEC04 - Final


## Getting started

En el presente proyecto presentamos una propuesta de videojuego usando los conocimientos adquiridos en la asignatura de programación 2D en Unity.

El concepto del juego está inspirado en un periodo de post guerra alrededor del mar Mediterráneo, y se enmarca en el género shoot'em up. El jugador controlará un vehículo aéreo con el que debe derribar a cuantos más enemigos mejor puntuación. La particularidad de nuestra propuesta es el desarrollo narrativo a través de diálogos estilo visual novel, a través de cuyas opciones de conversación, el jugador acabará en una u otra fase.

Puntos básicos:
## Splash Screen
Las primeras imágenes que se muestran al arrancar un juego suelen ser de imagen corporativa de sus responsables, y en el caso de un desarrollo con licencia gratuita de Unity, el logotipo de Unity. Esto se conoce como Splash Screen, y Unity posee una herramienta que facilita su implementación en:

          Player Settings -> Player -> Splash Image

Donde podemos incluir nuestros propios logotipos y configurar cómo se muestran.


## Pantalla de título y menú principal
En este caso, hemos decidido implementar ambos elementos en una primera escena. En ocasiones, la pantalla de título muestra un entorno tridimensional del juego en sí, pero en este caso estamos trabajando con las herramientas en 2D, así que esto será trabajado desde el mismo Canvas.
La pantalla de título es una imagen de presentación, y un texto parpadeante que indique pulsar cualquier botón. Hecho esto, la pantalla de título dejará paso al menú principal.

![Menu principal](https://gitlab.com/avilasumariva/pec04-final/-/raw/main/imgs/Captura%20de%20pantalla%202022-01-13%20225435.jpg)

Las opciones de preferencias se limitan al control del audio mixer
![opciones](https://gitlab.com/avilasumariva/pec04-final/-/raw/main/imgs/Captura%20de%20pantalla%202022-01-13%20225450.jpg)

Desde el menú principal, podemos comenzar a jugar, cambiar las opciones disponibles y salir del juego.



## Pantalla de diálogos
Comenzar el juego te enviará a la primera escena de diálogo. En este caso, no me ha sido posible desarrollar mis propios diseños, pero para ilustrar la temática ~~furra~~ de animales antropológicos, he usado renders del juego Smash Bros Ultimate.

El diálogo avanza pulsando espacion o boton izquierdo del ratón, con el cual tambien se seleccionarán las opciones si las hubiera.

![visual novel](https://gitlab.com/avilasumariva/pec04-final/-/raw/main/imgs/Captura%20de%20pantalla%202022-01-13%20225459.jpg)

En estas escenas se desarrollará sobre todo la historia del juego a través de diálogos que el jugador dirigirá para obtener distintos desenlaces y pasar por distintos niveles. En este caso, la implementación se enfoca en el canvas.

El área de texto principal funcionará como marco para la conversación, desde la que podemos ver el nombre del participante.
A los lados se muestran a los personajes reaccionando al diálogo.
En ocasiones, se mostrará al jugador opciones a elegir en forma de botones.

El sistema de diálogo se sustenta sobre un gestor de los elementos gráficos, y los diálogos en sí mismo. Como propuesta de mejora, hay que implementar el serializado de los diálogos y su lectura de ficheros externos. En este caso, se ha creado un script por cada sección de diálogo, para adaptarlos a las necesidades particulares de cada parte y adaptar un pequeño arbol de decisiones.


![Script de diálogos](https://gitlab.com/avilasumariva/pec04-final/-/raw/main/imgs/Captura%20de%20pantalla%202022-01-13%20230718.jpg)


## Pantalla de acción
Este tipo de niveles se estructura a través de la acción y el disparo, donde el jugador debe acabar con las ordas de enemigos para obtener la mejor puntuación.

El avión se mueve hacia los laterales usando las teclas A y B.
Dispara al pulsar el botón izquierdo del ratón.

![Escena shoot m up inicio](https://gitlab.com/avilasumariva/pec04-final/-/raw/main/imgs/Captura%20de%20pantalla%202022-01-13%20225512.jpg)

Hemos implementado una barra de vida, por la cual, al agotarse chocando con los enemigos, se acabará el juego. El objetivo del prototipo es alcanzar los mil puntos para completar el juego.

![combate](https://gitlab.com/avilasumariva/pec04-final/-/raw/main/imgs/Captura%20de%20pantalla%202022-01-13%20225517.jpg)

//Mejora de propuesta
Implementar input system

## Pantalla de créditos
Hemos optado por implementar los créditos como parte de la mecánica de disparo, donde el jugador puede disparar a los responsables del desarrollo, como homenaje a las pantallas de créditos de la saga Smash Bros.

![creditos](https://gitlab.com/avilasumariva/pec04-final/-/raw/main/imgs/redbox.png)


#El proyecto
Los elementos del proyecto estan organizados por secciones troncales o escenas, aunque hay recursos que se comparten.
![](https://gitlab.com/avilasumariva/pec04-final/-/raw/main/imgs/Captura%20de%20pantalla%202022-01-13%20225522.jpg)
